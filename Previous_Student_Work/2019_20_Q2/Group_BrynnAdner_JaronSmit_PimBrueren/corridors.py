__date__ = '28-11-2019'
__author__ = 'Shervin Azadi'
__author__ = 'Brynn Adnir'

node = hou.pwd()
geo = node.geometry()

#initiate the graph
import networkx as nx
G = nx.Graph()

#iterate over the points and add them as nodes of the graph
for point in geo.points():
    #retrieve the point number
    id = point.number()
    #add the node
    G.add_node(id)
    
#iterate over the prims and add them as edges of the graph
for prim in geo.prims():
    #retrieve the prim number
    id = prim.number()
    #getting the list of the points
    pnts = prim.points()
    #add the edge
    G.add_edge(pnts[0].number(),pnts[1].number())
    
# access the points in parentgroup (initially placed seeds) 
placed_seeds = geo.findPointGroup("parentgroup").points()

#import numpy
import numpy as np
# get connection data from file, and place in numpy array
data_path = "connectionmatrix.csv"
connection_weight = np.genfromtxt(data_path, delimiter=',', names=True, skip_header=0)
print(type(connection_weight))

paths_node_ids=[]
# find the shortest path between seeds
num_functions = len(placed_seeds)
#find the start seed ranging from function 0-15
path_counter = 0
for i in range (0, num_functions):
    start = placed_seeds[i].number()
    
    for j in range (i+1, num_functions):
        end = placed_seeds[j].number()
        # only looking to find path between seeds that should be directly connected
        # looking at connection weight between start seed(i) and end seed(j) - locating that in the connections matrix index [i][j]
        if connection_weight[i][j] > 0.5:
            #find the shortest path using A-star algorithm
            path_nodeid = nx.astar_path(G, start, end)
            paths_node_ids.append(path_nodeid)
            #retrieve the point object given the point number
            path_points = [geo.points()[id] for id in path_nodeid]
            # find the end seed ranging from start seed to func 15
            path_name = "path" + str(path_counter) 
            path_group = geo.createPointGroup(path_name)
            #group the points that are in the path
            path_group.add(path_points)
            path_counter += 1
print(paths_node_ids)