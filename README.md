# Spatial Computing

NOTE: we have moved all our repositories to GitHub, listed [here](https://genesis-lab.dev/courses/spatialcomputing/)

This repository will be kept for the time being as a legacy but eventually all our projects including Spatial Computing will be managed centrally through [Genesis Lab](https://www.tudelft.nl/en/architecture-and-the-built-environment/research/research-facilities/genesis-lab) in future. 

Spatial Computing is a computational design studio offered as a minor course to BSc students of TU Delft. The goal of this studio is to introduce basic mathematical and computational skills, especially in geometry, topology and graph theory (spatial mathematics), necessary for systematic analysis, synthesis, simulation, decision-making and optimization in architectural design. We propose a feed-forward way of designing in which the form is systematically derived from functional requirements. The goal of this studio is not to make a building in a ‘parametric style’, but to learn how to develop computational design workflows. Therefore, the main deliverable will be a demonstration of a functioning workflow showing the transformation of a site and a program of requirements into a building.
